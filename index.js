'use strict';
const fs = require('fs');
const Hapi = require('hapi');

const init = async () => {
    async function blobToBase64(blob) {
        const buffer = Buffer.from(blob, 'binary');
        return buffer.toString('base64');
    }

    async function decodeBase64(base64) {
        const response = {};
        const base64Data = base64.match(/^data:([A-Za-z-+/]+);base64,(.+)$/);
        const byteCharacters = Buffer.from(base64Data[2], 'base64').toString();
        response.blob = Buffer.from(byteCharacters, 'binary');
        [, response.type] = base64Data;

        return response;
    }

    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    server.route({
        method: 'GET',
        path:'/',
        handler: async (request, h) => {
            const folder = "./files/";
            let files = [];

            fs.readdirSync(folder).forEach(file => {
                console.log(file);
                files.push(file);
            });

            return files;
        }
    });

    server.route({
        method: 'GET',
        path:'/{file}',
        handler: async (request, h) => {
            const folder = "./files/";
            const fileName = request.params.file;

            const file = fs.readFileSync(`${folder}/${fileName}`)
            console.log(file);
            const ff = await blobToBase64(file);
            console.log(ff);
            return file;
        }
    });

    server.route({
        method: 'POST',
        path:'/',
        handler: (request, h) => {

            return '';
        }
    });

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();